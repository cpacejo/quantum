OPTFLAGS = -O3
CFLAGS = $(OPTFLAGS) -std=gnu11 -Wall -Werror

.DEFAULT_GOAL = qm_test

qm.o: qm.cu qm.h
qm-lib.o: qm-lib.c qm-lib.h qm.h
qm_test.o: qm_test.c qm.h qm-lib.h

%.o: %.cu
	nvcc $(OPTFLAGS) -o $@ -c $<

qm_test: qm.o qm-lib.o qm_test.o
	$(CC) $^ -lm -lcudart -o $@
