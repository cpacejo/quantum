#include <math.h>
#include "qm.h"

void qm_grover(void (*oracle)(qm_state_t *state, qm_reg_t reg), qm_state_t *state, qm_reg_t reg)
{
    qm_init(state, reg, 0u);
    qm_hadamard(state, QM_REG_NONE, reg);

    const int r =
        floor(scalbn(M_PI_4, reg.width / 2) *
            (reg.width % 2 == 0 ? 1.0 : M_SQRT2));

    for (int i = 0; i < r; i++)
    {
        oracle(state, reg);
        qm_gdiff(state, QM_REG_NONE, reg);
    }
}
