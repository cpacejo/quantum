#ifndef QM_H
#define QM_H

#include <stdio.h>

typedef struct qm_state_s qm_state_t;
typedef struct { int start, width; } qm_reg_t;

#define QM_REG(start, width) ((qm_reg_t) { (start), (width) })
#define QM_BIT(bit) QM_REG((bit), 1)
#define QM_REG_NONE QM_REG(0, 0)

#define QM_MAX_SIZE 31

// memory requirement is 16<<size bytes, plus change
qm_state_t *qm_alloc(int size);
void qm_free(qm_state_t *state);

unsigned int qm_measure(qm_state_t *state, qm_reg_t reg);
void qm_init(qm_state_t *state, qm_reg_t reg, unsigned int imm);

void qm_hadamard(qm_state_t *state, qm_reg_t control, qm_reg_t reg);
void qm_xori(qm_state_t *state, qm_reg_t control, qm_reg_t reg, unsigned int imm);
void qm_addi(qm_state_t *state, qm_reg_t control, qm_reg_t reg, int imm);
void qm_swap(qm_state_t *state, qm_reg_t control, qm_reg_t reg1, qm_reg_t reg2);
void qm_rol(qm_state_t *state, qm_reg_t control, qm_reg_t reg, int shift);
void qm_phase_shift(qm_state_t *state, qm_reg_t control, float phase);
void qm_multi_phase_shift(qm_state_t *state, qm_reg_t control, qm_reg_t reg, float phase);
void qm_clock(qm_state_t *state, qm_reg_t control, qm_reg_t reg, float phase);
void qm_gdiff(qm_state_t *state, qm_reg_t control, qm_reg_t reg);

void qm_dump(FILE* file, const qm_state_t *state);
void qm_dump_complex(FILE* file, const qm_state_t *state);

#endif
