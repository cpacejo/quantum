#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "qm.h"
#include "qm-lib.h"

static void oracle(qm_state_t *state, qm_reg_t reg)
{
    qm_addi(state, QM_REG_NONE, reg, -0x49);
    qm_phase_shift(state, reg, M_PI);
    qm_addi(state, QM_REG_NONE, reg, 0x49);
}


int main(int argc, char *argv[])
{
    int size = atoi(argv[1]);
    int reps = atoi(argv[2]);

    qm_state_t *state = qm_alloc(size);

    for (int i = 0; i < reps; i++)
    {
        qm_grover(oracle, state, QM_REG(0, 8));
        printf("%X\n", qm_measure(state, QM_REG(0, 8)));
    }

    qm_free(state);
    return 0;
}
