#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math_constants.h>
extern "C" {
#include "qm.h"
}

#define MAX_GRID_X_DIM_SHIFT 30
#define MAX_GRID_X_DIM (1 << MAX_GRID_DIM_SHIFT)

// hard max of 1024 threads per block
// max of 512 threads per block allows maximal placement into SMs on 8.6, 8.7, 8.9
// max of 256 threads per block allows maximal use of static shared memory on 8.7 and 9.0
// min of 128 threads per block required for maximal threads in SMs on 8.6 and 8.7
#define MAX_BLOCK_DIM_SHIFT 7
#define MAX_BLOCK_DIM (1 << MAX_BLOCK_DIM_SHIFT)

struct qm_state_s
{
    int size;
    float2 *mem, *temp;
};


//
// internal
//

static void check_reg(qm_state_t *state, qm_reg_t reg)
{
    assert(reg.start + reg.width <= state->size);
}


static void check_reg2(qm_state_t *state, qm_reg_t reg0, qm_reg_t reg1)
{
    check_reg(state, reg0);
    check_reg(state, reg1);
    assert(reg0.start + reg0.width <= reg1.start ||
           reg1.start + reg1.width <= reg0.start);
}


static unsigned int reg_mask(qm_reg_t reg)
{
    return (1u << (reg.start + reg.width)) - (1u << reg.start);
}


// compute row from thread + control
static __device__ unsigned int
insert_reg(unsigned int thread, qm_reg_t reg)
{
    return
        (thread & ((1u << reg.start) - 1u)) |
        ((thread & ~((1u << reg.start) - 1u)) << reg.width);
}


static __device__ unsigned int
remove_reg(unsigned int thread, qm_reg_t reg)
{
    return
        (thread & ((1u << reg.start) - 1u)) |
        ((thread & ~((1u << (reg.start + reg.width)) - 1u)) >> reg.width);
}


static __device__ unsigned int
insert_reg2(unsigned int thread, qm_reg_t reg0, qm_reg_t reg1)
{
    if (reg0.start < reg1.start)
        return insert_reg(insert_reg(thread, reg0), reg1);
    else
        return insert_reg(insert_reg(thread, reg1), reg0);
}


#if 0
static __device__ unsigned int
remove_reg2(unsigned int thread, qm_reg_t reg0, qm_reg_t reg1)
{
    if (reg0.start < reg1.start)
        return remove_reg(remove_reg(thread, reg1), reg0);
    else
        return remove_reg(remove_reg(thread, reg0), reg1);
}
#endif


static __device__ unsigned int
set_reg(unsigned int thread, qm_reg_t reg, unsigned int value)
{
    return
        thread |
        ((value & ((1u << reg.width) - 1u)) << reg.start);
}


static __device__ float2 cmul(float2 x, float2 y)
{
    return make_float2(x.x * y.x - x.y * y.y, x.x * y.y + x.y * y.x);
}


static __device__ float2 cpowi(float2 x, unsigned int k, int width)
{
    float2 ret = make_float2(CUDART_ONE_F, CUDART_ZERO_F);

    if ((k & 1u) != 0u)
    {
        ret.x = x.x;
        ret.y = x.y;
    }

    for (int i = 1; i < width; ++i)
    {
        k >>= 1;
        x = cmul(x, x);
        if ((k & 1u) != 0u)
            ret = cmul(x, ret);
    }

    return ret;
}


static void config_blocks(unsigned int size, dim3 *n_blocks)
{
    if (size < MAX_GRID_X_DIM_SHIFT)
    {
        n_blocks->x = 1u << size;
        n_blocks->y = 1u;
    }
    else
    {
        n_blocks->x = 1u << MAX_GRID_X_DIM_SHIFT;
        n_blocks->y = 1u << (size - MAX_GRID_X_DIM_SHIFT);
    }
}


static void config(int size, dim3 *n_blocks, dim3 *n_threads)
{
    if (size < MAX_BLOCK_DIM_SHIFT)
    {
        n_blocks->x = 1u;
        n_blocks->y = 1u;
        n_threads->x = 1u << size;
    }
    else
    {
        config_blocks(size - MAX_BLOCK_DIM_SHIFT, n_blocks);
        n_threads->x = 1u << MAX_BLOCK_DIM_SHIFT;
    }
}


static void swap_mem(qm_state_t *state)
{
    float2 *temp = state->temp;
    state->temp = state->mem;
    state->mem = temp;
}


//
// alloc/free
//

static __global__ void qm_init_kernel(float2 *mem, unsigned int val)
{
    unsigned int row = (blockIdx.y * gridDim.x + blockIdx.x) * blockDim.x + threadIdx.x;

    mem[row] = make_float2((row == val) ? CUDART_ONE_F : CUDART_ZERO_F, CUDART_ZERO_F);
}


qm_state_t *qm_alloc(int size)
{
    assert(size > 0);
    assert(size <= QM_MAX_SIZE);

    float2 *mem, *temp;
    {
        cudaError_t err = cudaMalloc(&mem, sizeof *mem << size);
        if (err != cudaSuccess) return NULL;
    }

    {
        cudaError_t err = cudaMalloc(&temp, sizeof *temp << size);
        if (err != cudaSuccess)
        {
            cudaFree(mem);
            return NULL;
        }
    }

    dim3 n_threads, n_blocks;
    config(size, &n_blocks, &n_threads);
    qm_init_kernel<<<n_blocks, n_threads>>>(mem, 0);

    qm_state_t *state = (qm_state_t *) malloc(sizeof(qm_state_t));
    state->size = size;
    state->mem = mem;
    state->temp = temp;
    return state;
}


void qm_free(qm_state_t *state)
{
    cudaFree(state->temp);
    cudaFree(state->mem);
    free(state);
}


//
// measure
//

static __global__ void qm_sumsq_kernel(
    const float2 *mem, float *res, int window_shift,
    qm_reg_t reg, unsigned int shifted_val)
{
    unsigned int thread =
        (((blockIdx.y * gridDim.x + blockIdx.x) * blockDim.x) << window_shift)
        + threadIdx.x;
    unsigned int window = 1u << window_shift;

    // Sum all entries where contents of reg == val

    float sumsq = CUDART_ZERO_F;

    for (unsigned int i = 0; i < window; i++)
    {
        unsigned int row = insert_reg(thread, reg) + shifted_val;

        float2 x = mem[row];
        sumsq += x.x * x.x + x.y * x.y;
        // stride; other threads in block access sequential addresses
        thread += blockDim.x;
    }

    __shared__ float sumsqs[MAX_BLOCK_DIM];
    sumsqs[threadIdx.x] = sumsq;
    __syncthreads();

    for (unsigned int step = blockDim.x >> 1; step > 0u; step >>= 1)
    {
        if (threadIdx.x < step)
            sumsqs[threadIdx.x] += sumsqs[threadIdx.x + step];
        __syncthreads();
    }

    if (threadIdx.x == 0u)
        res[blockIdx.y * gridDim.x + blockIdx.x] = sumsqs[0];
}


static __global__ void qm_search_kernel(const float *probs, float prob, unsigned int *res)
{
    __shared__ float probs_shared[MAX_BLOCK_DIM];
    probs_shared[threadIdx.x] = probs[threadIdx.x];
    __syncthreads();

    for (unsigned int step = blockDim.x >> 1; step > 0u; step >>= 1)
    {
        if (threadIdx.x < step)
            probs_shared[threadIdx.x] += probs_shared[threadIdx.x + step];
        __syncthreads();
    }

    prob *= probs_shared[0];

    unsigned int i = 0;
    for (unsigned int step = 1u; step < blockDim.x; step <<= 1)
    {
        if (prob < probs_shared[i + step]) i += step;
        else prob -= probs_shared[i + step];
    }

    *res = i;
}


static __global__ void qm_searchsq_kernel(const float2 *mem, float prob, unsigned int *res)
{
    __shared__ float probs_shared[MAX_BLOCK_DIM];
    float2 x = mem[threadIdx.x];
    probs_shared[threadIdx.x] = x.x * x.x + x.y * x.y;
    __syncthreads();

    for (unsigned int step = blockDim.x >> 1; step > 0u; step >>= 1)
    {
        if (threadIdx.x < step)
            probs_shared[threadIdx.x] += probs_shared[threadIdx.x + step];
        __syncthreads();
    }

    prob *= probs_shared[0];

    unsigned int i = 0;
    for (unsigned int step = 1u; step < blockDim.x; step <<= 1)
    {
        if (prob < probs_shared[i + step]) i += step;
        else prob -= probs_shared[i + step];
    }

    *res = i;
}


#define BUCKETS_SHIFT 8
#define BUCKETS (1u << BUCKETS_SHIFT)

#define WARP_DIM_SHIFT 5
#define WARP_DIM (1u << WARP_DIM_SHIFT)

static unsigned int sample(qm_state_t *state)
{
    unsigned int res = 0u;

    unsigned int partial_res, *partial_res_device;
    {
        cudaError_t err = cudaMalloc(&partial_res_device, sizeof *partial_res_device);
        assert(err == cudaSuccess);
    }

    // This is a weird algorithm, but all the linear/exponential parts are
    // parallelizable and everything else is log-time/linear.

    // Also, it ignores any scaling of the state vector.

    float *probs;
    {
        cudaError_t err = cudaMalloc(&probs, sizeof *probs << BUCKETS_SHIFT);
        assert(err == cudaSuccess);
    }

    // We measure BUCKETS_SHIFT qubits at once, high-order first.  Leave
    // room for parallelized memory accesses.

    int rem;
    for (rem = state->size; rem >= (BUCKETS_SHIFT + WARP_DIM_SHIFT); rem -= BUCKETS_SHIFT)
    {
        // First break up the problem so that each possible state for the
        // BUCKETS_SHIFT qubits we're measuring is assigned to a different
        // block.

        // We only have as many threads in a block as there are in a warp --
        // it costs time to sum across threads, but we do want to coalesce
        // memory accesses.

        // Each thread in the block sums linearly, then performs a log-time
        // sum across the threads in the block to produce a per-bucket sum.

        qm_sumsq_kernel<<<BUCKETS, WARP_DIM>>>(state->mem + (res << rem),
            probs, rem - (BUCKETS_SHIFT + WARP_DIM_SHIFT),
            QM_REG_NONE, 0u);
        cudaDeviceSynchronize();

        // Now we place each bucket into its own thread, and 
        // do a log-time running sum across buckets.  We can then
        // do a log-time search for the target state.

        float prob = (float) random() / (float) RAND_MAX;
        qm_search_kernel<<<1u, BUCKETS>>>(probs, prob, partial_res_device);
        cudaDeviceSynchronize();
        {
            cudaError_t err = cudaMemcpy(&partial_res, partial_res_device,
                sizeof partial_res, cudaMemcpyDeviceToHost);
            assert(err == cudaSuccess);
        }
        res = (res << BUCKETS_SHIFT) | partial_res;
    }

    // TODO: we could speed this up a bit by bailing out early if we don't
    // need extra cycles.  But in practice, we need at most 3, and on
    // average 2, so not a huge gain for the added complexity.

    // is > instead of >= to defer to simpler code below
    if (rem > BUCKETS_SHIFT)
    {
        qm_sumsq_kernel<<<BUCKETS, 1u << (rem - BUCKETS_SHIFT)>>>(
            state->mem + (res << rem), probs, 0, QM_REG_NONE, 0u);
        cudaDeviceSynchronize();

        float prob = (float) random() / (float) RAND_MAX;
        qm_search_kernel<<<1, BUCKETS>>>(probs, prob, partial_res_device);
        cudaDeviceSynchronize();
        {
            cudaError_t err = cudaMemcpy(&partial_res, partial_res_device,
                sizeof partial_res, cudaMemcpyDeviceToHost);
            assert(err == cudaSuccess);
        }
        res = (res << BUCKETS_SHIFT) | partial_res;

        rem -= BUCKETS_SHIFT;
    }

    assert(rem >= 0);
    float prob = (float) random() / (float) RAND_MAX;
    qm_searchsq_kernel<<<1, 1u << rem>>>(state->mem + (res << rem),
        prob, partial_res_device);
    cudaDeviceSynchronize();
    {
        cudaError_t err = cudaMemcpy(&partial_res, partial_res_device,
            sizeof partial_res, cudaMemcpyDeviceToHost);
        assert(err == cudaSuccess);
    }
    res = (res << rem) | partial_res;

    cudaFree(probs);
    cudaFree(partial_res_device);

    return res;
}


static __global__ void qm_collapse_kernel(float2 *mem, unsigned int mask, unsigned int shifted_val)
{
    unsigned int row = (blockIdx.y * gridDim.x + blockIdx.x) * blockDim.x + threadIdx.x;

    if ((row & mask) != shifted_val)
        mem[row] = make_float2(CUDART_ZERO_F, CUDART_ZERO_F);
}


static __global__ void qm_scale_kernel(float2 *mem, qm_reg_t reg, unsigned int shifted_val, float scale)
{
    unsigned int thread = (blockIdx.y * gridDim.x + blockIdx.x) * blockDim.x + threadIdx.x;
    unsigned int row = insert_reg(thread, reg) + shifted_val;

    float2 x = mem[row];
    mem[row] = make_float2(x.x * scale, x.y * scale);
}


static void collapse(qm_state_t *state, qm_reg_t reg, unsigned int shifted_val)
{
    // shortcut
    if (reg.width == state->size)
    {
        dim3 n_blocks, n_threads;
        config(state->size, &n_blocks, &n_threads);
        return qm_init_kernel<<<n_blocks, n_threads>>>(state->mem, shifted_val);
    }

    float prob, *prob_device;
    {
        cudaError_t err = cudaMalloc(&prob_device, sizeof *prob_device);
        assert(err == cudaSuccess);
    }

    // find the probability associated with this outcome
    // keep this simple; one block with a lot of threads
    // (we're memory bound & don't care about granularity in this algo)
    if (state->size - reg.width >= MAX_BLOCK_DIM_SHIFT)
        qm_sumsq_kernel<<<1u, MAX_BLOCK_DIM>>>(state->mem,
            prob_device, state->size - reg.width - MAX_BLOCK_DIM_SHIFT,
            reg, shifted_val);
    else
        qm_sumsq_kernel<<<1u, 1u << (state->size - reg.width)>>>(state->mem,
            prob_device, 0, reg, shifted_val);
    cudaDeviceSynchronize();

    {
        cudaError_t err = cudaMemcpy(&prob, prob_device,
            sizeof prob, cudaMemcpyDeviceToHost);
        assert(err == cudaSuccess);
    }
    cudaFree(prob_device);

    float scale = 1.0f / sqrtf(prob);

    // scale the consistent possibilities to total 100%
    {
        dim3 n_blocks, n_threads;
        config(state->size - reg.width, &n_blocks, &n_threads);
        qm_scale_kernel<<<n_blocks, n_threads>>>(state->mem, reg, shifted_val, scale);
    }

    // zero everything else
    {
        dim3 n_blocks, n_threads;
        config(state->size, &n_blocks, &n_threads);
        qm_collapse_kernel<<<n_blocks, n_threads>>>(state->mem, reg_mask(reg), shifted_val);
    }
}


unsigned int qm_measure(qm_state_t *state, qm_reg_t reg)
{
    check_reg(state, reg);

    if (reg.width == 0) return 0;

    cudaDeviceSynchronize();

    unsigned int res = sample(state);
    res &= reg_mask(reg);
    collapse(state, reg, res);
    return res >> reg.start;
}


void qm_init(qm_state_t *state, qm_reg_t reg, unsigned int imm)
{
    qm_xori(state, QM_REG_NONE, reg, qm_measure(state, reg) ^ imm);
}


//
// hadamard
//

static __global__ void qm_hadamard1_kernel(float2 *mem, qm_reg_t control, int bit)
{
    unsigned int thread = (blockIdx.y * gridDim.x + blockIdx.x) * blockDim.x + threadIdx.x;
    unsigned int row = set_reg(insert_reg2(thread, control, QM_BIT(bit)), control, -1u);
    unsigned int stride = 1u << bit;

    float2 a = mem[row], b = mem[row + stride];

    mem[row] = make_float2(CUDART_SQRT_HALF_F * (a.x + b.x), CUDART_SQRT_HALF_F * (a.y + b.y));
    mem[row + stride] = make_float2(CUDART_SQRT_HALF_F * (a.x - b.x), CUDART_SQRT_HALF_F * (a.y - b.y));
}


static __global__ void qm_hadamard2_kernel(float2 *mem, qm_reg_t control, int bit)
{
    unsigned int thread = (blockIdx.y * gridDim.x + blockIdx.x) * blockDim.x + threadIdx.x;
    unsigned int row = set_reg(insert_reg2(thread, control, QM_REG(bit, 2)), control, -1u);
    unsigned int stride = 1u << bit;

    float2 a = mem[row], b = mem[row + stride], c = mem[row + stride * 2u], d = mem[row + stride * 3u];

    mem[row] = make_float2(0.5f * (a.x + b.x + c.x + d.x), 0.5f * (a.y + b.y + c.y + d.y));
    mem[row + stride] = make_float2(0.5f * (a.x - b.x + c.x - d.x), 0.5f * (a.y - b.y + c.y - d.y));
    mem[row + stride * 2u] = make_float2(0.5f * (a.x + b.x - c.x - d.x), 0.5f * (a.y + b.y - c.y - d.y));
    mem[row + stride * 3u] = make_float2(0.5f * (a.x - b.x - c.x + d.x), 0.5f * (a.y - b.y - c.y + d.y));
}

/*
// for some reason this is super-slow
// requires blockDim.x == 256
static __global__ void qm_hadamard8_kernel(float2 *mem, qm_reg_t control, int bit)
{
    unsigned int row =
        set_reg(insert_reg2(blockIdx.x + blockIdx.y * gridDim.x, control, QM_REG(bit, 8)), control, -1u) +
        (threadIdx.x << bit);

    // read data into cache
    __shared__ float2 data[256];
    data[threadIdx.x] = mem[row];
    __syncthreads();

    // compute row sum
    float2 sum = make_float2(CUDART_ZERO_F, CUDART_ZERO_F);

    float pos_neg[2] = { CUDART_ONE_F, -CUDART_ONE_F };

    for (unsigned int i = 0; i < 256; i++)
    {
        // believe it or not this works
        unsigned int hadamard = __popc(threadIdx.x & i) & 1;

        sum.x += data[i].x * pos_neg[hadamard];
        sum.y += data[i].y * pos_neg[hadamard];
    }

    // scale
    sum.x *= 0.0625f;
    sum.y *= 0.0625f;

    // store
    mem[row] = sum;
}*/


void qm_hadamard(qm_state_t *state, qm_reg_t control, qm_reg_t reg)
{
    check_reg2(state, control, reg);

    if (reg.width == 0) return;

    cudaDeviceSynchronize();

    int bit = 0;

/*    if (bit < reg.width - 7)
    {
        unsigned int n_blocks = 1u << (state->size - (control.width + 8));
        dim3 grid_dim;
        if (n_blocks > MAX_GRID_DIM)
        {
            grid_dim.x = MAX_GRID_DIM;
            grid_dim.y = n_blocks >> MAX_GRID_DIM_SHIFT;
        }
        else
            grid_dim.x = n_blocks;

        for (; bit < reg.width - 7; bit += 8)
            qm_hadamard8_kernel<<<grid_dim, 256>>>(state->mem, control, reg.start + bit);
    }*/

    if (bit < reg.width - 1)
    {
        dim3 n_blocks, n_threads;
        config(state->size - (control.width + 2), &n_blocks, &n_threads);
        for (; bit < reg.width - 1; bit += 2)
            qm_hadamard2_kernel<<<n_blocks, n_threads>>>(state->mem, control, reg.start + bit);
    }

    if (bit < reg.width)
    {
        dim3 n_blocks, n_threads;
        config(state->size - (control.width + 1), &n_blocks, &n_threads);
        qm_hadamard1_kernel<<<n_blocks, n_threads>>>(state->mem, control, reg.start + bit);
    }
}


//
// xori
//

static __global__ void qm_xori_kernel(float2 *mem, qm_reg_t control, int bit, unsigned int shifted_imm)
{
    unsigned int thread = (blockIdx.y * gridDim.x + blockIdx.x) * blockDim.x + threadIdx.x;
    unsigned int row = set_reg(insert_reg2(thread, control, QM_BIT(bit)), control, -1u);

    float2 a = mem[row], b = mem[row ^ shifted_imm];

    mem[row] = b;
    mem[row ^ shifted_imm] = a;
}


void qm_xori(qm_state_t *state, qm_reg_t control, qm_reg_t reg, unsigned int imm)
{
    check_reg2(state, control, reg);

    // mask out inappropriate bits
    imm &= (1u << reg.width) - 1u;

    if (imm == 0u) return;

    cudaDeviceSynchronize();

    // right-align, so the kernel skips rows with rightmost 1 in mask
    int bit = reg.start + __builtin_ctz(imm);

    dim3 n_blocks, n_threads;
    config(state->size - (control.width + 1), &n_blocks, &n_threads);
    qm_xori_kernel<<<n_blocks, n_threads>>>(state->mem, control, bit, imm << reg.start);
}


//
// addi
//

static __global__ void qm_addi_kernel(const float2 *mem, float2 *res,
    qm_reg_t control, unsigned int mask, unsigned int shifted_imm)
{
    unsigned int thread = (blockIdx.y * gridDim.x + blockIdx.x) * blockDim.x + threadIdx.x;
    unsigned int row = set_reg(insert_reg(thread, control), control, -1u);

    res[(row & ~mask) | ((row + shifted_imm) & mask)] = mem[row];
}


static __global__ void qm_copy_kernel(const float2 *mem, float2 *res, qm_reg_t control)
{
    unsigned int thread = (blockIdx.y * gridDim.x + blockIdx.x) * blockDim.x + threadIdx.x;
    unsigned int row = set_reg(insert_reg(thread, control), control, -1u);

    res[row] = mem[row];
}


void qm_addi(qm_state_t *state, qm_reg_t control, qm_reg_t reg, int imm)
{
    check_reg2(state, control, reg);

    // mask out inappropriate bits
    unsigned int uimm = (unsigned int) imm & ((1u << reg.width) - 1u);

    if (uimm == 0u) return;

    cudaDeviceSynchronize();

    dim3 n_blocks, n_threads;
    config(state->size - control.width, &n_blocks, &n_threads);
    qm_addi_kernel<<<n_blocks, n_threads>>>(state->mem, state->temp,
        control, reg_mask(reg), uimm << reg.start);

    if (control.width > 0)
    {
        // this is faster than copying over non-modified states
        qm_copy_kernel<<<n_blocks, n_threads>>>(state->temp, state->mem, control);
    }
    else
    {
        // we already modified everything; just swap pointers
        swap_mem(state);
    }
}


//
// rol
//

static __global__ void qm_rol_kernel(const float2 *mem, float2 *res,
    qm_reg_t control, unsigned int mask, int width, int shift)
{
    unsigned int thread = (blockIdx.y * gridDim.x + blockIdx.x) * blockDim.x + threadIdx.x;
    unsigned int row = set_reg(insert_reg(thread, control), control, -1u);

    res[(row & ~mask) |
        ((((row & mask) << shift) | ((row & mask) >> (width - shift))) & mask)] =
        mem[row];
}


static __global__ void qm_ror_kernel(const float2 *mem, float2 *res,
    qm_reg_t control, unsigned int mask, int width, int shift)
{
    unsigned int thread = (blockIdx.y * gridDim.x + blockIdx.x) * blockDim.x + threadIdx.x;
    unsigned int row = set_reg(insert_reg(thread, control), control, -1u);

    res[(row & ~mask) |
        ((((row & mask) >> shift) | ((row & mask) << (width - shift))) & mask)] =
        mem[row];
}


void qm_rol(qm_state_t *state, qm_reg_t control, qm_reg_t reg, int shift)
{
    check_reg2(state, control, reg);

    if (shift == 0) return;

    if (shift > 0) shift = shift % reg.width;
    else shift = -((-shift) % reg.width);

    cudaDeviceSynchronize();

    dim3 n_blocks, n_threads;
    config(state->size - control.width, &n_blocks, &n_threads);
    if (shift > 0)
        qm_rol_kernel<<<n_blocks, n_threads>>>(state->mem, state->temp,
            control, reg_mask(reg), reg.width, shift);
    else
        qm_ror_kernel<<<n_blocks, n_threads>>>(state->mem, state->temp,
            control, reg_mask(reg), reg.width, -shift);

    if (control.width > 0)
    {
        // this is faster than copying over non-modified states
        qm_copy_kernel<<<n_blocks, n_threads>>>(state->temp, state->mem, control);
    }
    else
    {
        // we already modified everything; just swap pointers
        swap_mem(state);
    }
}


//
// swap
//

// WORKING HERE:
// trick is to use checkerboard pattern

/*static __global__ void qm_swap_kernel(float2 *mem,
    qm_reg_t control, unsigned int mask, int width, int shift)
{
    unsigned int thread = (blockIdx.y * gridDim.x + blockIdx.x) * blockDim.x + threadIdx.x;
    unsigned int row = set_reg(insert_reg(thread, control), control, -1u);

    res[(row & ~mask) |
        ((((row & mask) << shift) | ((row & mask) >> (width - shift))) & mask)] =
        mem[row];
}


void qm_swap(qm_state_t *state, qm_reg_t control, qm_reg_t reg1, qm_reg_t reg2)
{
    check_reg2(state, reg1, reg2);
    assert(reg1.width == reg2.width);

    cudaDeviceSynchronize();

    dim3 n_blocks, n_threads;
    config(state->size - control.width, &n_blocks, &n_threads);
    qm_rol_kernel<<<n_blocks, n_threads>>>(state->mem,
        control, reg_mask(reg), reg.width, );
}*/


//
// phase shift
//

static __global__ void qm_phase_shift_kernel(float2 *mem, qm_reg_t control, float s, float c)
{
    unsigned int thread = (blockIdx.y * gridDim.x + blockIdx.x) * blockDim.x + threadIdx.x;
    unsigned int row = set_reg(insert_reg(thread, control), control, -1u);

    mem[row] = cmul(make_float2(c, s), mem[row]);
}


void qm_phase_shift(qm_state_t *state, qm_reg_t control, float phase)
{
    check_reg(state, control);

    float s, c;
    sincosf(phase, &s, &c);

    cudaDeviceSynchronize();

    dim3 n_blocks, n_threads;
    config(state->size - control.width, &n_blocks, &n_threads);
    qm_phase_shift_kernel<<<n_blocks, n_threads>>>(state->mem, control, s, c);
}


//
// multi phase shift
//

static __global__ void qm_multi_phase_shift1_kernel(float2 *mem, qm_reg_t control, unsigned int bit, float s, float c)
{
    unsigned int thread = (blockIdx.y * gridDim.x + blockIdx.x) * blockDim.x + threadIdx.x;
    unsigned int row = set_reg(insert_reg2(thread, control, QM_BIT(bit)), control, -1u);

    // phase shift when set
    row += 1 << bit;

    mem[row] = cmul(make_float2(c, s), mem[row]);
}


static __global__ void qm_multi_phase_shift_kernel(float2 *mem, qm_reg_t control, unsigned int mask, float s, float c)
{
    unsigned int thread = (blockIdx.y * gridDim.x + blockIdx.x) * blockDim.x + threadIdx.x;
    unsigned int row = set_reg(insert_reg(thread, control), control, -1u);

    // the number of 1 bits set in the mask is the phase multiplier
    float2 rot = cpowi(make_float2(c, s), __popc(row & mask), 6);

    mem[row] = cmul(rot, mem[row]);
}


void qm_multi_phase_shift(qm_state_t *state, qm_reg_t control, qm_reg_t reg, float phase)
{
    check_reg2(state, control, reg);

    float s, c;
    sincosf(phase, &s, &c);

    if (reg.width == 1)
    {
        cudaDeviceSynchronize();

        dim3 n_blocks, n_threads;
        config(state->size - (control.width + 1), &n_blocks, &n_threads);
        qm_multi_phase_shift1_kernel<<<n_blocks, n_threads>>>(state->mem, control, reg.start, s, c);
    }
    else if (reg.width > 1)
    {
        cudaDeviceSynchronize();

        dim3 n_blocks, n_threads;
        config(state->size - control.width, &n_blocks, &n_threads);
        qm_multi_phase_shift_kernel<<<n_blocks, n_threads>>>(state->mem, control, reg_mask(reg), s, c);
    }
}


//
// clock
//

static __global__ void qm_clock_kernel(float2 *mem, qm_reg_t control, qm_reg_t reg, float s, float c)
{
    unsigned int thread = (blockIdx.y * gridDim.x + blockIdx.x) * blockDim.x + threadIdx.x;
    unsigned int row = set_reg(insert_reg(thread, control), control, -1u);

    // the register value is the phase multiplier
    float2 rot = cpowi(make_float2(c, s), row >> reg.start, reg.width);

    mem[row] = cmul(rot, mem[row]);
}


void qm_clock(qm_state_t *state, qm_reg_t control, qm_reg_t reg, float phase)
{
    check_reg2(state, control, reg);

    float s, c;
    sincosf(phase, &s, &c);

    if (reg.width == 1)
    {
        cudaDeviceSynchronize();

        dim3 n_blocks, n_threads;
        config(state->size - (control.width + 1), &n_blocks, &n_threads);
        qm_multi_phase_shift1_kernel<<<n_blocks, n_threads>>>(state->mem, control, reg.start, s, c);
    }
    else if (reg.width > 1)
    {
        cudaDeviceSynchronize();

        dim3 n_blocks, n_threads;
        config(state->size - control.width, &n_blocks, &n_threads);
        qm_clock_kernel<<<n_blocks, n_threads>>>(state->mem, control, reg, s, c);
    }
}


//
// gdiff
//

// used when reg.width <= MAX_BLOCK_DIM_SHIFT
// one thread per entry in res
static __global__ void qm_sum_narrow_kernel(
    const float2 *mem, float2 *res, qm_reg_t control, qm_reg_t reg)
{
    unsigned int sum_row = (blockIdx.y * gridDim.x + blockIdx.x) * blockDim.x + threadIdx.x;
    unsigned int row = set_reg(insert_reg2(sum_row, reg, control), control, -1u);

    float2 sum = mem[row];

    // reg.width is small; no need to bother making this more symmetric
    for (unsigned int i = 1; i < 1u << reg.width; i++)
    {
        row += 1u << reg.start;

        float2 x = mem[row];
        sum.x += x.x;
        sum.y += x.y;
    }

    res[sum_row] = make_float2(
        scalbnf(sum.x, 1 - reg.width),
        scalbnf(sum.y, 1 - reg.width));
}


// used when reg.width >= MAX_BLOCK_DIM_SHIFT
// one block per entry in res
// blockDim.x * window == 1 << reg.width
static __global__ void qm_sum_wide_kernel(
    const float2 *mem, float2 *res, unsigned int window,
    qm_reg_t control, qm_reg_t reg)
{
    unsigned int sum_row = blockIdx.y * gridDim.x + blockIdx.x;
    unsigned int row = set_reg(set_reg(insert_reg2(sum_row, reg, control), reg, threadIdx.x), control, -1u);

    float2 sum = mem[row];

    for (unsigned int i = 1; i < window; i++)
    {
        // stride; other threads in block access sequential addresses
        row += blockDim.x << reg.start;

        float2 x = mem[row];
        sum.x += x.x;
        sum.y += x.y;
    }

    __shared__ float2 sums[MAX_BLOCK_DIM];
    sums[threadIdx.x] = sum;

    // drop the other warps
    __syncthreads();
    if (threadIdx.x >= WARP_DIM) return;

    // first sum across warps
    // NOTE: assumes blockDim.x % WARP_DIM = 0!
    for (unsigned int step = WARP_DIM; step < blockDim.x; step += WARP_DIM)
    {
        float2 other = sums[threadIdx.x + step];
        sum.x += other.x;
        sum.y += other.y;
    }

    // now sum within our warp
    // NOTE: assumes blockDim.x >= WARP_DIM!
    for (unsigned int step = WARP_DIM >> 1; step > 0u; step >>= 1)
    {
        sums[threadIdx.x] = sum;
        __syncwarp((step << 1) - 1u);

        if (threadIdx.x >= step) return;

        float2 other = sums[threadIdx.x + step];
        sum.x += other.x;
        sum.y += other.y;
    }

    res[sum_row] = make_float2(
        scalbnf(sum.x, 1 - reg.width),
        scalbnf(sum.y, 1 - reg.width));
}


static __global__ void qm_gdiff_kernel(float2 *mem, const float2 *twice_avgs, qm_reg_t control, qm_reg_t reg)
{
    unsigned int thread = (blockIdx.y * gridDim.x + blockIdx.x) * blockDim.x + threadIdx.x;
    unsigned int row = set_reg(insert_reg(thread, control), control, -1u);
    unsigned int avg_row =
        control.start < reg.start ?
            remove_reg(thread, QM_REG(reg.start - control.width, reg.width)) :
            remove_reg(thread, reg);

    float2 twice_avg = twice_avgs[avg_row], x = mem[row];
    mem[row] = make_float2(twice_avg.x - x.x, twice_avg.y - x.y);
}


void qm_gdiff(qm_state_t *state, qm_reg_t control, qm_reg_t reg)
{
    check_reg2(state, control, reg);

    if (reg.width == 0) return;

    if (reg.width <= MAX_BLOCK_DIM_SHIFT)
    {
        // narrow registers -- sum within one thread
        dim3 n_blocks, n_threads;
        config(state->size - (control.width + reg.width), &n_blocks, &n_threads);

        cudaDeviceSynchronize();
        qm_sum_narrow_kernel<<<n_blocks, n_threads>>>(state->mem, state->temp, control, reg);
    }
    else
    {
        // wide registers -- sum within one block
        dim3 n_blocks;
        config_blocks(state->size - (control.width + reg.width), &n_blocks);

        cudaDeviceSynchronize();
        qm_sum_wide_kernel<<<n_blocks, 1u << MAX_BLOCK_DIM_SHIFT>>>(state->mem, state->temp,
            1u << (reg.width - MAX_BLOCK_DIM_SHIFT), control, reg);
    }
    // TODO: very wide registers -- sum across blocks
    // we don't produce more than 320 blocks?  (i.e. "idler" bits < 9) or some other N
    // window gets very long?  (pick a figure -- 128?)  <-- this is better, indicates lots of work to do
    //   ...then just, split window across blocks -- atomic add into sum array (and zero init) -- but otherwise identical

    {
        dim3 n_blocks, n_threads;
        config(state->size - control.width, &n_blocks, &n_threads);

        cudaDeviceSynchronize();
        qm_gdiff_kernel<<<n_blocks, n_threads>>>(state->mem, state->temp, control, reg);
    }
}


//
// dump
//

#define DUMP_BUFSIZE_SHIFT 10
#define DUMP_BUFSIZE (1u << DUMP_BUFSIZE_SHIFT)

static __global__ void qm_dump_kernel(const float2 *mem, float2 *res)
{
    unsigned int row = (blockIdx.y * gridDim.x + blockIdx.x) * blockDim.x + threadIdx.x;

    float2 x = mem[row];
    res[row] = make_float2(
        100.0f * (x.x * x.x + x.y * x.y),
        atan2f(x.x, x.y) * (180.0f / CUDART_PI_F));
}


void qm_dump(FILE* file, const qm_state_t *state)
{
    {
        dim3 n_blocks, n_threads;
        config(state->size, &n_blocks, &n_threads);

        cudaDeviceSynchronize();
        qm_dump_kernel<<<n_blocks, n_threads>>>(state->mem, state->temp);
    }

    float2 buf[DUMP_BUFSIZE];

    cudaDeviceSynchronize();

    if (state->size < DUMP_BUFSIZE_SHIFT)
    {
        {
            cudaError_t err = cudaMemcpy(buf, state->temp,
                sizeof buf[0] << state->size, cudaMemcpyDeviceToHost);
            assert(err == cudaSuccess);
        }

        for (unsigned int i = 0; i < 1u << state->size; i++)
        {
            float2 x = buf[i];
            fprintf(file, "%06X: %.2f%% @ %.0f°\n", i, x.x, x.y);
        }
    }
    else for (unsigned int i = 0; i < 1u << state->size; i += DUMP_BUFSIZE)
    {
        {
            cudaError_t err = cudaMemcpy(buf, state->temp,
                DUMP_BUFSIZE, cudaMemcpyDeviceToHost);
            assert(err == cudaSuccess);
        }

        for (unsigned int j = 0; j < 1u << state->size; j++)
        {
            float2 x = buf[i + j];
            fprintf(file, "%06X: %.2f%% @ %.0f°\n", i + j, x.x, x.y);
        }
    }
}


void qm_dump_complex(FILE* file, const qm_state_t *state)
{
    float2 buf[DUMP_BUFSIZE];

    cudaDeviceSynchronize();

    if (state->size < DUMP_BUFSIZE_SHIFT)
    {
        {
            cudaError_t err = cudaMemcpy(buf, state->mem,
                sizeof buf[0] << state->size, cudaMemcpyDeviceToHost);
            assert(err == cudaSuccess);
        }

        for (unsigned int i = 0; i < 1u << state->size; i++)
        {
            float2 x = buf[i];
            fprintf(file, "%06X: %.4f + %.4fi\n", i, x.x, x.y);
        }
    }
    else for (unsigned int i = 0; i < 1u << state->size; i += DUMP_BUFSIZE)
    {
        {
            cudaError_t err = cudaMemcpy(buf, state->mem,
                DUMP_BUFSIZE, cudaMemcpyDeviceToHost);
            assert(err == cudaSuccess);
        }

        for (unsigned int j = 0; j < 1u << state->size; j++)
        {
            float2 x = buf[i + j];
            fprintf(file, "%06X: %.4f + %.4fi\n", i + j, x.x, x.y);
        }
    }
}
